package com.pomtech.yasin.memoriesnotebook2.model;


import android.net.Uri;

/**
 * Created by M.Yasin on 4/13/2017.
 */

public class Memory {

    static  int counter = 0;
    private  int id = 0;
    private  String name;
    private String date;
    private  String description;
    private String location;
    private boolean isGood;
    private Uri uri;

    public Memory(int id ,String name, String location,  String description, Uri uri ,String date) {
        this.name = name;
        this.date = date;
        this.description = description;
        this.location = location;
        this.uri = uri;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isGood() {
        return isGood;
    }

    public void setGood(boolean good) {
        isGood = good;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        String memory = "Name : " + getName() + "       " +
                        "Location : " + getLocation() + "     " +
                        "Description : " + getDescription() + "     " +
                        "Date : " + getDate() + "       " ;
        return memory;
    }
}
