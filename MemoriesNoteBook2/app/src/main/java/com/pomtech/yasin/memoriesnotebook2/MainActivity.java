package com.pomtech.yasin.memoriesnotebook2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.pomtech.yasin.memoriesnotebook2.dataBase.DBFunctions;
import com.pomtech.yasin.memoriesnotebook2.model.Memory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    static ListView listView;
    Adapter adapter;
    ArrayAdapter<Memory> arrayAdapter ;
    Intent intent;
    ArrayList<Memory> memories;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //
        new DBFunctions(this);
        memories = new DBFunctions(this).selectMemories();

        listView = (ListView) findViewById(R.id.listView);


        adapter = new Adapter(getApplicationContext() , memories);
//        arrayAdapter = adapter;

        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, final long id) {
//                final Memory m = memories.get(position);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

                alertDialog.setMessage("Choose what to happen?");

                alertDialog.setNeutralButton( "Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        intent = new Intent(MainActivity.this , EditActivity.class);

                                intent.putExtra("name" , memories.get(position).getName());
                                intent.putExtra("location" , memories.get(position).getLocation());
                                intent.putExtra("description" , memories.get(position).getDescription());
                                intent.putExtra("date" , memories.get(position).getDate());
                                intent.putExtra("id" , memories.get(position).getId());
                                Bundle bundle = new Bundle();
                                bundle.putParcelable("uri" , memories.get(position).getUri());
                                intent.putExtra("uri" , bundle);
                        startActivity(intent);

                    }
                });

                alertDialog.setNegativeButton("Export", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String fileName = "fileName";

                        FileOutputStream fileOutputStream;

                        try {
                            fileOutputStream = openFileOutput(fileName , getApplicationContext().MODE_PRIVATE);
                            fileOutputStream.write(memories.get(position - 1).toString().getBytes());
                            fileOutputStream.close();

                            Toast.makeText(getApplicationContext(), position + " exported" ,Toast.LENGTH_LONG).show();

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });

                alertDialog.setPositiveButton("Delete" , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

//                                Memories.memories.remove((int)id);
//                        Toast.makeText(getApplicationContext() ,m.getId() + "" ,Toast.LENGTH_LONG);
                        new DBFunctions(getApplicationContext()).deleteMemory(memories.get(position).getId());
                        memories.remove(position);
                        listView.setAdapter(new Adapter(getApplicationContext() ,memories));
//                        Toast.makeText(getApplicationContext() ,"Deleted" + m.getId()  , Toast.LENGTH_LONG).show();
                    }
                });

                alertDialog.show();
                return true;
            }
        });

        //buttons
        Button add = (Button) findViewById(R.id.addButton);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intent = new Intent(MainActivity.this , AddActivity.class);
                startActivity(intent);
            }
        });


        final Button search = (Button) findViewById(R.id.searchButton);
        final EditText searchEditText = (EditText) findViewById(R.id.search_edit_frame);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Memory> searched = new ArrayList<Memory>();
                for( int i = 0 ; i < memories.size() ; i++){
                    if( memories.get(i).getName().equalsIgnoreCase(searchEditText.getText().toString().trim())){
                        searched.add( memories.get(i));
                    }
                }

                if(searchEditText.getText().toString().trim().equalsIgnoreCase("")){
                    memories = new DBFunctions(getApplicationContext()).selectMemories();
                    listView.setAdapter(new Adapter(getApplicationContext() , memories));
                }else {
                    listView.setAdapter(new Adapter(getApplicationContext(), searched));
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menubar , menu);

        MenuItem menuItem = menu.findItem(R.id.saerchBar);
        Toast.makeText(this , menuItem.toString() , Toast.LENGTH_LONG).show();
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ArrayList<Memory> serchedMemos = new DBFunctions(getApplicationContext()).serachMemories(newText);
//                adapter.getFilter().filter(newText);
                listView.setAdapter(new Adapter(getApplicationContext() , serchedMemos));
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
