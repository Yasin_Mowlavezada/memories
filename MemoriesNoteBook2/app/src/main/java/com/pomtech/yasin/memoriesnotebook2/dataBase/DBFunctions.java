package com.pomtech.yasin.memoriesnotebook2.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.pomtech.yasin.memoriesnotebook2.model.Memory;

import java.util.ArrayList;

/**
 * Created by M.Yasin on 4/24/2017.
 */

public class DBFunctions {
    ArrayList<Memory> memories;
    DataBaseConfig dataBaseConfig;

    public DBFunctions(Context context){
        dataBaseConfig = new DataBaseConfig(context);
        dataBaseConfig.getWritableDatabase();
    }

    //ADD FUNCTION

    public void addMemory(String name, String location , String description , String uri ,String date){
        SQLiteDatabase db = dataBaseConfig.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(DataBaseConfig.TITLE , name);
        contentValues.put(DataBaseConfig.LOCATION , location);
        contentValues.put(DataBaseConfig.DESCRIPTION , description);
        contentValues.put(DataBaseConfig.URI , uri);
        contentValues.put(DataBaseConfig.DATE , date);

        db.insert(DataBaseConfig.TABLE_NAME , null ,contentValues);
        db.close();
    }

    public ArrayList<Memory> selectMemories(){
        SQLiteDatabase db = dataBaseConfig.getReadableDatabase();

        String[] columnNames = {DataBaseConfig.ID ,DataBaseConfig.TITLE , DataBaseConfig.LOCATION,
                DataBaseConfig.DESCRIPTION , DataBaseConfig.URI,DataBaseConfig.DATE };

        memories = new ArrayList<>();

        Cursor cursor = db.query(DataBaseConfig.TABLE_NAME ,columnNames , null ,null ,null ,null ,null);

        while (cursor.moveToNext()){
            memories.add(new Memory(cursor.getInt(cursor.getColumnIndex(DataBaseConfig.ID)),
                                    cursor.getString(cursor.getColumnIndex(DataBaseConfig.TITLE)),
                                    cursor.getString(cursor.getColumnIndex(DataBaseConfig.LOCATION )),
                                    cursor.getString(cursor.getColumnIndex(DataBaseConfig.DESCRIPTION)),
                            Uri.parse(cursor.getString(cursor.getColumnIndex(DataBaseConfig.URI))),
                                    cursor.getString(cursor.getColumnIndex(DataBaseConfig.DATE))));
        }

        return memories;
    }

    public void deleteMemory(int id){
        SQLiteDatabase db = dataBaseConfig.getWritableDatabase();

        String where = DataBaseConfig.ID + " = ? ";
        String[] whereArgs = { id + "" } ;

        db.delete(DataBaseConfig.TABLE_NAME ,where,whereArgs);
        db.close();
    }

    public void editMemory(int id , String name , String location , String description , String uri , String date){
        SQLiteDatabase db = dataBaseConfig.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(DataBaseConfig.TITLE , name);
        contentValues.put(DataBaseConfig.LOCATION , location);
        contentValues.put(DataBaseConfig.DESCRIPTION , description);
        contentValues.put(DataBaseConfig.URI , uri);
        contentValues.put(DataBaseConfig.DATE , date);

        String where = DataBaseConfig.ID + " = ? ";
        String[] whereArgs = { id + "" } ;

        db.update(DataBaseConfig.TABLE_NAME , contentValues ,where,whereArgs);
        db.close();
    }

    public ArrayList<Memory> serachMemories(String name){
        ArrayList<Memory> searchedMemos = new ArrayList<>();
        SQLiteDatabase db = dataBaseConfig.getReadableDatabase();
        String[] columnNames = {DataBaseConfig.ID ,DataBaseConfig.TITLE , DataBaseConfig.LOCATION,
                DataBaseConfig.DESCRIPTION , DataBaseConfig.URI,DataBaseConfig.DATE };


        String where = DataBaseConfig.TITLE + " = ? %";
        String[] whereArgs = {name};
        Cursor cursor = db.query(DataBaseConfig.TABLE_NAME ,columnNames ,where ,whereArgs ,null ,null ,null);

        while (cursor.moveToNext()){
            searchedMemos.add(new Memory(cursor.getInt(cursor.getColumnIndex(DataBaseConfig.ID)),
                    cursor.getString(cursor.getColumnIndex(DataBaseConfig.TITLE)),
                    cursor.getString(cursor.getColumnIndex(DataBaseConfig.LOCATION )),
                    cursor.getString(cursor.getColumnIndex(DataBaseConfig.DESCRIPTION)),
                    Uri.parse(cursor.getString(cursor.getColumnIndex(DataBaseConfig.URI))),
                    cursor.getString(cursor.getColumnIndex(DataBaseConfig.DATE))));
        }

        return searchedMemos;
    }
}
