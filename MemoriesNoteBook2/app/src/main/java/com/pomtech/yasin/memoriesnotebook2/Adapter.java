package com.pomtech.yasin.memoriesnotebook2;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.pomtech.yasin.memoriesnotebook2.model.Memory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by M.Yasin on 4/17/2017.
 */

public class Adapter extends BaseAdapter implements Filterable{

    Context context;
    ArrayList<Memory> memories;
    public  Adapter(Context context , ArrayList<Memory> memories){
        this.memories = memories;
        this.context = context;
    }

    @Override
    public int getCount() {
        return memories.size();
    }

    @Override
    public Object getItem(int position) {
        return memories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.list_item , null , false);

        }

        TextView name = (TextView) convertView.findViewById(R.id.name);
        name.setText(memories.get(position).getName());
        TextView decs= (TextView) convertView.findViewById(R.id.date);
        decs.setText(String.valueOf(memories.get(position).getDate()));
        ImageView imageView = (ImageView) convertView.findViewById(R.id.image);
        imageView.setImageURI(memories.get(position).getUri());

        return convertView;
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults filterResults = new FilterResults();
                ArrayList<Memory> filteredArrList = new ArrayList<>();

                for (int j = 0 ; j < memories.size() ; j++){
                    if(memories.get(j).getName().toLowerCase().startsWith(constraint.toString())){
                        filteredArrList.add(memories.get(j));
                    }
                }

                filterResults.count = filteredArrList.size();
                filterResults.values = filteredArrList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                memories = (ArrayList<Memory>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }
}

