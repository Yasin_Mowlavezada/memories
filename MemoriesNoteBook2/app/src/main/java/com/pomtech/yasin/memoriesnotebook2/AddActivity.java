package com.pomtech.yasin.memoriesnotebook2;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.pomtech.yasin.memoriesnotebook2.dataBase.DBFunctions;

/**
 * Created by M.Yasin on 4/17/2017.
 */

public class AddActivity extends AppCompatActivity {

    String imagePath;
    private final static int SELECT_PHOTO = 12345;
    ImageView imageView;
    Uri pickedImage;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_layout);

        //Buttons
        Button addButton = (Button) findViewById(R.id.addButton);
        final EditText nameText = (EditText) findViewById(R.id.nameEditText);
        final EditText location = (EditText) findViewById(R.id.locationEditText);
        final EditText date = (EditText) findViewById(R.id.dateEditText);
        final EditText description = (EditText) findViewById(R.id.description);

        imageView = (ImageView) findViewById(R.id.image);

        final CheckBox isGood = (CheckBox) findViewById(R.id.isGoodBox);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nameString = nameText.getText().toString();
                String dateLong = date.getText().toString();
                String descriptionString = description.getText().toString();
                String locationString = location.getText().toString();
                boolean isGoodBoolean = isGood.isSelected();

                if(nameString.trim().equalsIgnoreCase("") || date.getText().toString().trim().equalsIgnoreCase("")
                        || descriptionString.trim().equalsIgnoreCase("")||locationString.trim().equalsIgnoreCase("")
                        || pickedImage == null){
                    Toast.makeText(getApplicationContext() , "Fill all the fields" , Toast.LENGTH_LONG).show();
                }else {
                    new DBFunctions(getApplicationContext()).addMemory(nameString ,locationString ,
                                descriptionString ,pickedImage.toString() ,dateLong);
                    nameText.setText("");
                    location.setText("");
                    date.setText("");
                    description.setText("");
                    imageView.setImageResource(R.drawable.ic_account_circle_black_48dp);
                }

                MainActivity.listView.setAdapter(new Adapter(getApplicationContext() , new DBFunctions(getApplicationContext()).selectMemories()));
                finish();
            }
        });




    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Here we need to check if the activity that was triggers was the Image Gallery.
        // If it is the requestCode will match the LOAD_IMAGE_RESULTS value.
        // If the resultCode is RESULT_OK and there is some data we know that an image was picked.
        if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK && data != null) {
            // Let's read picked image data - its URI

            pickedImage = data.getData();
            // Let's read picked image path using content resolver
            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
            cursor.moveToFirst();
            imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            imageView.setImageURI(pickedImage);
            // Do something with the bitmap

            imageView.setVisibility(View.VISIBLE);
            // At the end remember to close the cursor or you will end with the RuntimeException!
            cursor.close();
        }
    }

    public void pickImage(View view){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }
}
