package com.pomtech.yasin.memoriesnotebook2.dataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by M.Yasin on 4/24/2017.
 */

public class DataBaseConfig extends SQLiteOpenHelper {

    public static final String DATA_BASE_NAME = "MemoriesDataBase.db";
    public static final int VERSION = 1 ;

    public static final String TABLE_NAME = "MEMORY_TABLE";
    public static final String TITLE = "title";
    public static final String LOCATION = "location";
    public static final String DATE = "date";
    public static final String DESCRIPTION = "description";
    public static final String URI = "uri";
    public static final String ID = "_id";

    public String CREATE_MEORIES_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
            +ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
            +TITLE + " TEXT , "
            +LOCATION + " TEXT , "
            +DATE + " TEXT , "
            +DESCRIPTION + " TEXT , "
            +URI + " TEXT " +
            ");";

    public String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    public DataBaseConfig(Context context) {
        super(context ,DATA_BASE_NAME ,null , VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        try {
            db.execSQL(CREATE_MEORIES_TABLE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL(DROP_TABLE);
            onCreate(db);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
