package com.pomtech.yasin.memoriesnotebook2;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.pomtech.yasin.memoriesnotebook2.dataBase.DBFunctions;

/**
 * Created by M.Yasin on 4/17/2017.
 */

public class EditActivity extends AppCompatActivity {

    String imagePath;
    private final static int SELECT_PHOTO = 12345;
    ImageView imageView;
    Uri pickedImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_layout);

        final EditText name = (EditText) findViewById(R.id.editNameText);
        final EditText location = (EditText) findViewById(R.id.editLocationText);
        final EditText description = (EditText) findViewById(R.id.editDescription);
        final EditText date = (EditText) findViewById(R.id.editDateText);

        final CheckBox isGood = (CheckBox) findViewById(R.id.isGoodEditBox);
        imageView = (ImageView) findViewById(R.id.editImage);

        Button edit = (Button) findViewById(R.id.editButton);

        final Intent intent = getIntent();

        if (intent != null) {

            name.setText(intent.getStringExtra("name"));
            location.setText(intent.getStringExtra("location"));
            description.setText(intent.getStringExtra("description"));
            date.setText(intent.getStringExtra("date"));
            imageView.setImageURI((Uri) intent.getExtras().getBundle("uri").getParcelable("uri"));
            isGood.setChecked(intent.getBooleanExtra("isGood", false));
            pickedImage = intent.getBundleExtra("uri").getParcelable("uri");
        }



        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int id = intent.getIntExtra("id" , -1);
                if(id == -1 || name.getText().toString().trim().equalsIgnoreCase("")
                        || location.getText().toString().trim().equalsIgnoreCase("")
                        || description.getText().toString().trim().equalsIgnoreCase("")
                        || date.getText().toString().trim().equalsIgnoreCase("")){

                    Toast.makeText(getApplicationContext() , "Fill all fields" , Toast.LENGTH_LONG).show();

                }else{
                    new DBFunctions(getApplicationContext()).editMemory(id ,name.getText().toString() ,location.getText().toString()
                            , description.getText().toString() ,pickedImage.toString() ,date.getText().toString());
                    MainActivity.listView.setAdapter(new Adapter(getApplicationContext() ,
                            new DBFunctions(getApplicationContext()).selectMemories()));
                    finish();
                    Toast.makeText(getApplicationContext() , id+"" , Toast.LENGTH_LONG).show();
                }
            }
        });


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Here we need to check if the activity that was triggers was the Image Gallery.
        // If it is the requestCode will match the LOAD_IMAGE_RESULTS value.
        // If the resultCode is RESULT_OK and there is some data we know that an image was picked.
        if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK && data != null) {
            // Let's read picked image data - its URI

            pickedImage = data.getData();
            // Let's read picked image path using content resolver
            String[] filePath = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(pickedImage, filePath, null, null, null);
            cursor.moveToFirst();
            imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            imageView.setImageURI(pickedImage);
            // Do something with the bitmap

            imageView.setVisibility(View.VISIBLE);
            // At the end remember to close the cursor or you will end with the RuntimeException!
            cursor.close();
        }
    }

    public void pickImage(View view){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }
}